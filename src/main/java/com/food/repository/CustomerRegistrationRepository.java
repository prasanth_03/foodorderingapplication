package com.food.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.food.DTO.LoginStatusEnum;
import com.food.entity.CustomerRegistration;

@Repository
public interface CustomerRegistrationRepository extends JpaRepository<CustomerRegistration, String> {

	Optional<CustomerRegistration> findByEmail(String email);
	
	Optional<CustomerRegistration> findByCustomerIdAndLoginStatus(String customerId, LoginStatusEnum loginStatus );

	CustomerRegistration findByEmailAndPassword(String email, String password);

	Optional<CustomerRegistration> findByCustomerId(String customerId);

}
