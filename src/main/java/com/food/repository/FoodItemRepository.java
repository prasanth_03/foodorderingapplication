package com.food.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.food.entity.FoodItem;

@Repository
public interface FoodItemRepository extends JpaRepository<FoodItem, String> {

	boolean existsByFoodName(String foodName);

	List<FoodItem> findByFoodNameContainingIgnoreCaseOrFoodTypeContainingIgnoreCaseOrShopNameContainingIgnoreCase(
			String foodName, String foodType, String shopName);

	Optional<FoodItem> findByFoodId(String foodId);

	

	

}