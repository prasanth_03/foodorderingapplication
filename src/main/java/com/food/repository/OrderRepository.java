package com.food.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.food.DTO.PaymentStatus;
import com.food.entity.Orders;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long> {

	List <Orders> findByCustomerIdAndPaymentStatus(String customerId, PaymentStatus paymentStatus);

	boolean existsByCustomerCustomerId(String customerId);
	
}
