package com.food.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import com.food.DTO.LoginDTO;
import com.food.DTO.LoginStatusEnum;
import com.food.entity.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {
	   
	Optional<Login> findByEmailAndPassword(String email, String password);

	void save(LoginDTO loginDTO);

	Optional<Login> findByEmailAndLoginStatus(String email, LoginStatusEnum loginStatus);

	}
