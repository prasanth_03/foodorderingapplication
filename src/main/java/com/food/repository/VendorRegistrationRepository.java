package com.food.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.food.DTO.LoginStatusEnum;
import com.food.entity.VendorRegistration;
import com.food.entity.FoodItem;

@Repository
public interface VendorRegistrationRepository extends JpaRepository<VendorRegistration, String> {

	Optional<VendorRegistration> findByEmail(String email);
	
	Optional<VendorRegistration> findByVendorIdAndLoginStatus(String vendorId, LoginStatusEnum loginStatus );

	VendorRegistration findByEmailAndPassword(String email, String password);

	

}
