package com.food.DTO;

import org.hibernate.validator.constraints.Range;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record FoodItemRecord(@NotBlank(message = "Food Name is required field") String foodName,
		@NotBlank(message = "Description is required field") String description,
		@NotBlank(message = "Food Type is required field") String foodType,
		@NotNull(message = "Food Price is required field")
		@Range(min = (long) 10.0, max = (long) 200.0) double foodPrice ){

}
