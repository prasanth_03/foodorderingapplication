package com.food.DTO;

public enum LoginStatusEnum {

	LOGGED_IN,
	LOGGED_OUT
}
