package com.food.DTO;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class OrdersDTO {

	@NotBlank(message = "User Id is required field")
	private String customerId;
	@NotBlank(message = "Food Id is required field")
	private String foodId;
	@NotBlank(message = "Food Quantity must be atleast 1")
	private int quantity;
}
