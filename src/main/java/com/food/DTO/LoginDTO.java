package com.food.DTO;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginDTO {

	@NotBlank(message = "Email ID cannot be empty")
	private String email;
	@NotBlank(message = "Password cannot be empty")
	private String password;
}
