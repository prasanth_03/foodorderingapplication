package com.food.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VendorResponseDTO {
	
	private String message;
	private String shopName;
}
