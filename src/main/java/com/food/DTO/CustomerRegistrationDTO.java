package com.food.DTO;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@Builder
@ToString
@NoArgsConstructor
public class CustomerRegistrationDTO {

	@NotBlank(message = "Name is requiredfield")
	private String name;
	
	@Email(message = "Please Enter a valid email")
	private String email;
	
	@NotNull(message = "Contact number is a required field")
	@Pattern(regexp = "[6789][\\d]{9}", message = "Enter a valid indian contact number")
	private String contactNo;

	@NotNull(message = "password should not be null")
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]).{8,}$", message = "Password must be at least 8 characters long and include at least one digit, one lowercase letter, one uppercase letter, and one special character.")
	private String password;
}
