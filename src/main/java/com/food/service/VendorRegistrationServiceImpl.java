package com.food.service;

import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.DTO.LoginStatusEnum;
import com.food.DTO.VendorRegistrationDTO;
import com.food.DTO.ResponseDTO;
import com.food.DTO.Roles;
import com.food.entity.VendorRegistration;
import com.food.exception.UserAlreadyExists;
import com.food.repository.VendorRegistrationRepository;

@Service
public class VendorRegistrationServiceImpl implements VendorRegistrationService {

	@Autowired
	public VendorRegistrationRepository registrationRepository;

	public VendorRegistrationServiceImpl(VendorRegistrationRepository registrationRepository) {
		super();
		this.registrationRepository = registrationRepository;
	}
	
	public ResponseDTO registration(VendorRegistrationDTO registrationdto) {
		
		Optional<VendorRegistration> userRegistration = registrationRepository.findByEmail(registrationdto.getEmail());
		if (userRegistration.isPresent()) {
			throw new UserAlreadyExists("Vendor already Registered");
		}
		
		VendorRegistration registration = new VendorRegistration();
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2 = "VID" + id;
		registration.setVendorId(id2);
		registration.setName(registrationdto.getName());
		registration.setShopName(registrationdto.getShopName());
		registration.setEmail(registrationdto.getEmail());
		registration.setPassword(registrationdto.getPassword());
		registration.setContactNo(registrationdto.getContactNo());
		registration.setRole(Roles.VENDOR);
		registration.setLoginStatus(LoginStatusEnum.LOGGED_OUT);
		registrationRepository.save(registration);
		return new ResponseDTO(Collections.singletonList("Vendor Registration successfully..."), 201);
	}
}
