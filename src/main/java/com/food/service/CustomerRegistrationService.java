package com.food.service;

import com.food.DTO.CustomerRegistrationDTO;
import com.food.DTO.ResponseDTO;

public interface CustomerRegistrationService {

	public ResponseDTO registration(CustomerRegistrationDTO registrationdto);
}
