package com.food.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.food.DTO.LoginStatusEnum;
import com.food.DTO.Roles;
import com.food.entity.CustomerRegistration;
import com.food.entity.Login;
import com.food.entity.VendorRegistration;
import com.food.repository.CustomerRegistrationRepository;
import com.food.repository.LoginRepository;
import com.food.repository.VendorRegistrationRepository;

@Service
public class LoginService {
   @Autowired
   private CustomerRegistrationRepository customerRegistrationRepository;
   @Autowired
   private VendorRegistrationRepository vendorRepository;
   @Autowired
   private LoginRepository loginRepository;
   public ResponseEntity<?> login(String email, String password) {
       Optional<CustomerRegistration> customerRegistrationOptional = customerRegistrationRepository.findByEmail(email);
       Optional<VendorRegistration> vendorOptional = vendorRepository.findByEmail(email);
       
       Optional<Login> loggedInUser = loginRepository.findByEmailAndLoginStatus(email, LoginStatusEnum.LOGGED_IN);
       if (loggedInUser.isPresent()) {
           if (customerRegistrationOptional.isPresent() && customerRegistrationOptional.get().getPassword().equals(password)) {
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User already logged in");
           } else if (vendorOptional.isPresent() && vendorOptional.get().getPassword().equals(password)) {
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vendor already logged in");
           }
       }
       
       
       if (customerRegistrationOptional.isPresent() && customerRegistrationOptional.get().getPassword().equals(password)) {
 
           loginRepository.save(new Login(email, password, Roles.USER, LoginStatusEnum.LOGGED_IN));
           CustomerRegistration customer = customerRegistrationOptional.get();
           customer.setLoginStatus(LoginStatusEnum.LOGGED_IN);
           customerRegistrationRepository.save(customer);
           return ResponseEntity.ok("Customer login successful!");
       } else if (vendorOptional.isPresent() && vendorOptional.get().getPassword().equals(password)) {
    	   loginRepository.save(new Login(email, password, Roles.VENDOR, LoginStatusEnum.LOGGED_IN));
    	   VendorRegistration register = vendorOptional.get();
    	   register.setLoginStatus(LoginStatusEnum.LOGGED_IN);
    	   vendorRepository.save(register);
           return ResponseEntity.ok("Vendor login successful!");
       } else {
           return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid email/password");
       }
   }
}
