package com.food.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.food.DTO.LoginStatusEnum;
import com.food.DTO.FoodItemRecord;
import com.food.DTO.VendorResponseDTO;
import com.food.entity.VendorRegistration;
import com.food.entity.FoodItem;
import com.food.exception.ResourceNotFound;
import com.food.repository.VendorRegistrationRepository;
import com.food.repository.FoodItemRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class FoodItemService {

	private FoodItemRepository vendorRepository;

	private VendorRegistrationRepository registrationRepository;

	public VendorResponseDTO addMenu(String vendorId, FoodItemRecord vendorMenuRecord) {
		Optional<VendorRegistration> registrationOptional = registrationRepository.findByVendorIdAndLoginStatus(vendorId,
				LoginStatusEnum.LOGGED_IN);

		if (registrationOptional.isPresent()) {
			VendorRegistration registration = registrationOptional.get();

			if (vendorRepository.existsByFoodName(vendorMenuRecord.foodName())) {
				throw new ResourceNotFound("Food with the same name is already exists");
			}

			FoodItem vendor = new FoodItem();
			String characters2 = "0123456789";
			String id = RandomStringUtils.random(6, characters2);
			String id2 = "FID" + id;
			vendor.setFoodId(id2);
			vendor.setFoodName(vendorMenuRecord.foodName());
			vendor.setDescription(vendorMenuRecord.description());
			vendor.setFoodType(vendorMenuRecord.foodType());
			vendor.setFoodPrice(vendorMenuRecord.foodPrice());
			vendor.setShopName(registration.getShopName());
			vendor.setRegistration(registration);
			vendorRepository.save(vendor);

			String message = "Food details Added Successfully...";
			VendorResponseDTO responseDTO = new VendorResponseDTO(message, registration.getShopName());
			return responseDTO;
		} else {
			throw new ResourceNotFound("Please Login to add Food menu details");
		}
	}

	public List<FoodItem> searchMenu(String value) {
		return vendorRepository.findByFoodNameContainingIgnoreCaseOrFoodTypeContainingIgnoreCaseOrShopNameContainingIgnoreCase(value, value, value);
	}
}
