package com.food.service;

import com.food.DTO.VendorRegistrationDTO;
import com.food.DTO.ResponseDTO;

public interface VendorRegistrationService {

	ResponseDTO registration(VendorRegistrationDTO registrationdto);
}
