package com.food.service;

import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.DTO.CustomerRegistrationDTO;
import com.food.DTO.LoginStatusEnum;
import com.food.DTO.ResponseDTO;
import com.food.DTO.Roles;
import com.food.entity.CustomerRegistration;
import com.food.exception.UserAlreadyExists;
import com.food.repository.CustomerRegistrationRepository;

@Service
public class CustomerRegistrationServiceImpl implements CustomerRegistrationService {
	
	@Autowired
	public CustomerRegistrationRepository registrationRepository;
	
	
	public CustomerRegistrationServiceImpl(CustomerRegistrationRepository registrationRepository) {
		super();
		this.registrationRepository = registrationRepository;
	}
	
	public ResponseDTO registration(CustomerRegistrationDTO registrationdto) {
		
		Optional<CustomerRegistration> userRegistration = registrationRepository.findByEmail(registrationdto.getEmail());
		if (userRegistration.isPresent()) {
			throw new UserAlreadyExists("User already Registered");
		}
		
		CustomerRegistration registration = new CustomerRegistration();
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2 = "UID" + id;
		registration.setCustomerId(id2);
		registration.setName(registrationdto.getName());
		registration.setEmail(registrationdto.getEmail());
		registration.setPassword(registrationdto.getPassword());
		registration.setContactNo(registrationdto.getContactNo());
		registration.setRole(Roles.USER);
		registration.setLoginStatus(LoginStatusEnum.LOGGED_OUT);
		registrationRepository.save(registration);
		return new ResponseDTO(Collections.singletonList("User Registration successfully..."), 201);
	}
}