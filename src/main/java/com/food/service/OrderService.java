package com.food.service;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.food.DTO.OrderDTO;
import com.food.DTO.OrdersDTO;
import com.food.DTO.PaymentStatus;
import com.food.entity.CustomerRegistration;
import com.food.entity.Orders;
import com.food.entity.FoodItem;
import com.food.exception.NotFoundException;
import com.food.repository.CustomerRegistrationRepository;
import com.food.repository.OrderRepository;
import lombok.AllArgsConstructor;
import com.food.repository.FoodItemRepository;

@AllArgsConstructor
@Service
public class OrderService {
   
   private OrderRepository orderRepository;
   
   private CustomerRegistrationRepository customerRepository;
   
   private FoodItemRepository foodItemRepository;
   
   public void placeOrder(OrdersDTO orderDTO) {
       
       CustomerRegistration customer = customerRepository.findByCustomerId(orderDTO.getCustomerId()).orElseThrow(() -> new NotFoundException("Customer not found"));
       
       FoodItem foodItem = foodItemRepository.findByFoodId(orderDTO.getFoodId()).orElseThrow(() -> new NotFoundException("Food item not found"));

       double totalPrice = orderDTO.getQuantity() * foodItem.getFoodPrice();
       
       Orders order = new Orders();
       order.setCustomer(customer);
       order.setFoodItem(foodItem);
       order.setQuantity(orderDTO.getQuantity());
       order.setTotalPrice(totalPrice);
       order.setPaymentStatus(PaymentStatus.PENDING);
      
       orderRepository.save(order);
   }
   
   public OrderDetailsResponse getOrderDetails(String customerId) {
	   boolean customerExists = orderRepository.existsByCustomerCustomerId(customerId);
       if (!customerExists) {
           throw new NotFoundException("Customer not found");
       }
	   
       List<Orders> orders = orderRepository.findByCustomerIdAndPaymentStatus(customerId, PaymentStatus.PENDING);
       List<OrderDTO> orderDTOs = orders.stream().map(this::mapToOrderDTO).collect(Collectors.toList());
       double totalAmountToPay = orders.stream().mapToDouble(order -> order.getQuantity() * order.getFoodItem().getFoodPrice()).sum();
       return new OrderDetailsResponse(orderDTOs, totalAmountToPay);
   }
   private OrderDTO mapToOrderDTO(Orders order) {
       OrderDTO orderDTO = new OrderDTO();
       orderDTO.setCustomerId(order.getCustomer().getCustomerId());
       orderDTO.setFoodId(order.getFoodItem().getFoodId());
       orderDTO.setFoodName(order.getFoodItem().getFoodName());
       orderDTO.setFoodPrice(order.getFoodItem().getFoodPrice());
       orderDTO.setQuantity(order.getQuantity());
       double totalPrice = order.getQuantity() * order.getFoodItem().getFoodPrice();
       orderDTO.setTotalPrice(totalPrice);
       return orderDTO;
   }
   
   public static class OrderDetailsResponse {
       private List<OrderDTO> orderDTOs;
       private double totalAmountToPay;
       public OrderDetailsResponse(List<OrderDTO> orderDTOs, double totalAmountToPay) {
           this.orderDTOs = orderDTOs;
           this.totalAmountToPay = totalAmountToPay;
       }
       public List<OrderDTO> getOrderDTOs() {
           return orderDTOs;
       }
       public double getTotalAmountToPay() {
           return totalAmountToPay;
       }
   }
   public boolean processPayment(String customerId, double totalAmountToPay) {
       
       boolean customerExists = orderRepository.existsByCustomerCustomerId(customerId);
       if (!customerExists) {
           throw new NotFoundException("Customer not found");
       }
       
       List<Orders> orders = orderRepository.findByCustomerIdAndPaymentStatus(customerId, PaymentStatus.PENDING);
       
       double totalAmountFromOrders = orders.stream()
               .mapToDouble(order -> order.getQuantity() * order.getFoodItem().getFoodPrice())
               .sum();
       
       if (totalAmountFromOrders == totalAmountToPay) {
           
           orders.forEach(order -> {
               order.setPaymentStatus(PaymentStatus.SUCCESS);
               orderRepository.save(order);
           });
           System.out.println("Payment successful");
           return true;
       } else {
           System.out.println("Payment failed: Total amount doesn't match");
           return false;
       }
   }
}