package com.food.entity;

import com.food.DTO.PaymentStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Table(name = "orders")
public class Orders {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long orderId;
   
   @Column(insertable=false, updatable=false)
   private String customerId;
   
   
   @ManyToOne
   @JoinColumn(name = "customerId" , referencedColumnName = "customerId")
   private CustomerRegistration customer;
   
   @ManyToOne
   @JoinColumn(name = "food_id")
   private FoodItem foodItem;
   
   private int quantity;
   private double totalPrice;
   
   @Enumerated(EnumType.STRING)
   private PaymentStatus paymentStatus;
   // getters and setters
}