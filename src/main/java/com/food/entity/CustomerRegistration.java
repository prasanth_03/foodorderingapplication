package com.food.entity;

import com.food.DTO.LoginStatusEnum;
import com.food.DTO.Roles;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRegistration {

	@Id
	private String customerId;
	private String name;
	private String email;
	private String contactNo;
	private String password;
	
	@Enumerated(EnumType.STRING)
	private Roles role;
	
	@Enumerated(EnumType.STRING)
	private LoginStatusEnum loginStatus;
}
