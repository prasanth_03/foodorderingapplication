package com.food.entity;

import com.food.DTO.LoginStatusEnum;
import com.food.DTO.Roles;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VendorRegistration {


	@Id
	private String vendorId;
	private String name;
	private String shopName;
	private String email;
	
	private String contactNo;
	private String password;
	
	@Enumerated(EnumType.STRING)
	private Roles role;
	
	@Enumerated(EnumType.STRING)
	private LoginStatusEnum loginStatus;
	
	public VendorRegistration(String string, String string2, String string3, String string4, LoginStatusEnum loggedIn) {
	}

}
