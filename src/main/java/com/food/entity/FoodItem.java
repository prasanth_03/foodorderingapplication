package com.food.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class FoodItem {
	
	@Id
	private String foodId;
	private String foodName;
	private String description;
	private String foodType;
	private double foodPrice;
	
	@Column(name = "shopName")
	private String shopName;

	
	@ManyToOne
	private VendorRegistration registration;


}
