package com.food.entity;

import com.food.DTO.LoginStatusEnum;
import com.food.DTO.Roles;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@Entity
public class Login {


	public Login(String email, String password, Roles role, LoginStatusEnum loginStatus) {
		super();
		this.email = email;
		this.password = password;
		this.role = role;
		this.loginStatus = loginStatus;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String email;
	private String password;

	@Enumerated(EnumType.STRING)
	private Roles role;
	
	@Enumerated(EnumType.STRING)
	private LoginStatusEnum loginStatus;
}
