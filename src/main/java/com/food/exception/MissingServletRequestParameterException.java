package com.food.exception;

public class MissingServletRequestParameterException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MissingServletRequestParameterException(String message) {
		super(message);

	}

	public MissingServletRequestParameterException() {
		super();
	}
}