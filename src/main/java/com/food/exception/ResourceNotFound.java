package com.food.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResourceNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public ResourceNotFound(String message) {
		super();
		this.message = message;
	}
}