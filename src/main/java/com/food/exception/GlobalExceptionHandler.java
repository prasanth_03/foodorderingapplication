package com.food.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import com.food.DTO.ResponseDTO;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleinvaliddata(MethodArgumentNotValidException ex) {
		Map<String, String> errMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors().forEach(err -> {
			errMap.put(err.getField(), err.getDefaultMessage());
		});
		return errMap;
	}
	
	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<Object> handleUserAlreadyExists(UserAlreadyExists ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(ex.getLocalizedMessage());
		ResponseDTO response = new ResponseDTO(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ProfileNotFoundException.class)
	public ResponseEntity<ResponseDTO> handleProfileNotFoundException(ProfileNotFoundException exception, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(exception.getLocalizedMessage());
		ResponseDTO response = new ResponseDTO(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	} 
	
	@ExceptionHandler(ResourceNotFound.class)
	public ResponseEntity<ResponseDTO> handleResourceNotFound(ResourceNotFound exception, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(exception.getLocalizedMessage());
		ResponseDTO response = new ResponseDTO(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	   @ResponseStatus(HttpStatus.BAD_REQUEST)
	   public ResponseEntity<String> handleMissingParams(MissingServletRequestParameterException ex) {
	       return ResponseEntity.badRequest().body(ex.getMessage());
	   }
	
	@ExceptionHandler(NotFoundException.class)
	   @ResponseStatus(HttpStatus.NOT_FOUND)
	   public String handleNotFoundException(NotFoundException ex) {
	       return ex.getMessage();
	   }
	
	@ExceptionHandler(InvalidArgumentException.class)
	   public ResponseEntity<String> handleInvalidArgumentException(InvalidArgumentException ex) {
	       return ResponseEntity.badRequest().body("Error: " + ex.getMessage());
	   }
}
