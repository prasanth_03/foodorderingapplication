package com.food.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.DTO.VendorRegistrationDTO;
import com.food.DTO.ResponseDTO;
import com.food.service.VendorRegistrationServiceImpl;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping()
public class VendorRegistrationController {

	//@Autowired
	VendorRegistrationServiceImpl registrationServiceImpl;
	
	@PostMapping("/vendorregistration")
	public ResponseEntity<ResponseDTO> register(@Valid @RequestBody VendorRegistrationDTO resgistrationDto) {
		return new ResponseEntity<ResponseDTO>(registrationServiceImpl.registration(resgistrationDto),
				HttpStatus.CREATED);
	}
}
