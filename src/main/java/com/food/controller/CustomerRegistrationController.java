package com.food.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.food.DTO.CustomerRegistrationDTO;
import com.food.DTO.ResponseDTO;
import com.food.service.CustomerRegistrationServiceImpl;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping()
public class CustomerRegistrationController {

	CustomerRegistrationServiceImpl registrationServiceImpl;
	
	@PostMapping("/userregistration")
	public ResponseEntity<ResponseDTO> register(@Valid @RequestBody CustomerRegistrationDTO resgistrationDto) {
		return new ResponseEntity<ResponseDTO>(registrationServiceImpl.registration(resgistrationDto),
				HttpStatus.CREATED);
	}
}
