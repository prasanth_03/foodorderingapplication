package com.food.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.service.LoginService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/login")
@Validated
public class LoginController {

	private LoginService loginService;

	@PostMapping
	   public ResponseEntity<?> login(@RequestParam String email, @RequestParam String password) {
	       return loginService.login(email, password);
	   }
}