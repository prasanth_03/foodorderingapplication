package com.food.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.DTO.OrdersDTO;
import com.food.exception.NotFoundException;
import com.food.service.OrderService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/api/orders")
public class OrderController {
   
   private OrderService orderService;
   @PostMapping("/placeOrder")
   public ResponseEntity<String> placeOrder(@RequestBody OrdersDTO orderDTO) {
       orderService.placeOrder(orderDTO);
       return ResponseEntity.ok("Order placed successfully");
   }
   
   @GetMapping("/details/{customerId}")
   public ResponseEntity<OrderService.OrderDetailsResponse> getOrderDetails(@PathVariable String customerId) {
       OrderService.OrderDetailsResponse orderDetailsResponse = orderService.getOrderDetails(customerId);
       return new ResponseEntity<>(orderDetailsResponse, HttpStatus.OK);
   }
   @PostMapping("/orders/process-payment")
   public ResponseEntity<String> processPayment(@RequestParam String customerId, @RequestParam double totalAmountToPay) {
       try {
           boolean paymentSuccess = orderService.processPayment(customerId, totalAmountToPay);
           if (paymentSuccess) {
               return ResponseEntity.ok("Payment successful");
           } else {
               return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Payment failed: Total amount doesn't match");
           }
       } catch (NotFoundException e) {
           return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found");
       } catch (Exception e) {
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to process payment");
       }
   }
}