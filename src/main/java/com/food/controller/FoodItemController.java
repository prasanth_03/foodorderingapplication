package com.food.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.DTO.FoodItemRecord;
import com.food.DTO.VendorResponseDTO;
import com.food.entity.FoodItem;
import com.food.exception.NotFoundException;
import com.food.service.FoodItemService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/foodmenu")
public class FoodItemController {

	@Autowired
	private FoodItemService vendorService;
	
	

	public FoodItemController(FoodItemService vendorService) {
		this.vendorService = vendorService;
	}

	@PostMapping("/{vendorId}")
	public ResponseEntity<VendorResponseDTO> addBeneficiary(@PathVariable String vendorId,
			@RequestBody @Valid FoodItemRecord vendorMenuRecord) {

		VendorResponseDTO responsedto = vendorService.addMenu(vendorId, vendorMenuRecord);
		return new ResponseEntity<>(responsedto, HttpStatus.CREATED);
	}
	
	@GetMapping("/search")
	   public List<FoodItem> searchMenu(@RequestParam String value) {
	       List<FoodItem> menus = vendorService.searchMenu(value);
	       if (menus.isEmpty()) {
	           throw new NotFoundException("No menus found for the given search value");
	       }
	       return menus;
	   }
}