package com.food.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.food.DTO.ResponseDTO;
import com.food.DTO.VendorRegistrationDTO;
import com.food.controller.VendorRegistrationController;
import com.food.service.VendorRegistrationServiceImpl;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

class VendorRegistrationControllerTest {
	@Mock
	private VendorRegistrationServiceImpl registrationService;
	@InjectMocks
	private VendorRegistrationController registrationController;
	private Validator validator;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	void testRegister_Failure() {
		VendorRegistrationDTO registrationDto = new VendorRegistrationDTO();
		// Set invalid data to trigger validation errors
		registrationDto.setName(null);
		ResponseEntity<ResponseDTO> response = registrationController.register(registrationDto);
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test
	void testRegister_Failure_InvalidData() {
		VendorRegistrationDTO registrationDto = new VendorRegistrationDTO();
		registrationDto.setName(null);
		ResponseEntity<ResponseDTO> response = registrationController.register(registrationDto);
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}
}