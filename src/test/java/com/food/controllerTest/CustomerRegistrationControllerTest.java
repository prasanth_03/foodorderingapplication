package com.food.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.food.DTO.CustomerRegistrationDTO;
import com.food.DTO.ResponseDTO;
import com.food.controller.CustomerRegistrationController;
import com.food.service.CustomerRegistrationServiceImpl;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerRegistrationControllerTest {
	@Mock
	private CustomerRegistrationServiceImpl registrationService;
	@InjectMocks
	private CustomerRegistrationController registrationController;
	private MockMvc mockMvc;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(registrationController).build();
	}
	
	@Test
	void testAddRegistrationSuccess() {
		CustomerRegistrationDTO customerRegistrationDTO = new CustomerRegistrationDTO();
		customerRegistrationDTO.setName("Yash");
		customerRegistrationDTO.setEmail("yash@gmail.com");
		customerRegistrationDTO.setPassword("Yash@78");
		customerRegistrationDTO.setContactNo("8976479329");
		ResponseDTO responseDto = new ResponseDTO();
		responseDto.setHttpStatus(201);
		Mockito.when(registrationService.registration(customerRegistrationDTO)).thenReturn(responseDto);
		ResponseEntity<ResponseDTO> register = registrationController.register(customerRegistrationDTO);
		assertEquals(201, register.getBody().getHttpStatus());
	}
	@Test
	void testAddRegistrationUnSuccess() {
		CustomerRegistrationDTO customerRegistrationDTO = new CustomerRegistrationDTO();
		customerRegistrationDTO.setName("Yash");
		customerRegistrationDTO.setEmail("yash@gmail.com");
		customerRegistrationDTO.setPassword("Yash@78");
		customerRegistrationDTO.setContactNo("8976479329");
		ResponseDTO responseDto = new ResponseDTO();
		responseDto.setHttpStatus(201);
		Mockito.when(registrationService.registration(customerRegistrationDTO)).thenReturn(responseDto);
		ResponseEntity<ResponseDTO> register = registrationController.register(customerRegistrationDTO);
		assertNotEquals(202, register.getBody().getHttpStatus());
	}
}
