package com.food.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.food.controller.LoginController;
import com.food.service.LoginService;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {

	@InjectMocks
	private LoginController loginController;

	@Mock
	private LoginService loginService;

	@Test
	void testAddLogin_Successful() {

		String email = "yash@gmail.com";
		String password = "Yash@78";

		when(loginService.login(email, password)).thenReturn(new ResponseEntity<>(HttpStatus.OK));

		ResponseEntity<?> registerEntity = loginController.login(email, password);

		assertEquals(HttpStatus.OK, registerEntity.getStatusCode());

	}

	@Test
	void testAddLogin_UnSuccessful() {

		String email = "yash@gmail.com";
		String password = "Yash@78";

		when(loginService.login(email, password)).thenReturn(new ResponseEntity<>(HttpStatus.OK));

		ResponseEntity<?> registerEntity = loginController.login(email, password);

		assertEquals(HttpStatus.OK, registerEntity.getStatusCode());

	}

}