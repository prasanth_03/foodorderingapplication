package com.food.controllerTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.food.DTO.FoodItemRecord;
import com.food.DTO.VendorResponseDTO;
import com.food.controller.FoodItemController;
import com.food.entity.FoodItem;
import com.food.exception.NotFoundException;
import com.food.service.FoodItemService;

class FoodItemControllerTest {
	@Mock
	private FoodItemService foodItemService;
	@InjectMocks
	private FoodItemController foodItemController;

	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testAddBeneficiary_Success() {
		String customerId = "vendor123";
		FoodItemRecord foodItemRecord = new FoodItemRecord("Pizza", "Delicious pizza", "Italian", 15.99);
		VendorResponseDTO expectedResponseDTO = new VendorResponseDTO("Food details Added Successfully...",
				"Pizza Shop");
		when(foodItemService.addMenu(customerId, foodItemRecord)).thenReturn(expectedResponseDTO);
		ResponseEntity<VendorResponseDTO> responseEntity = foodItemController.addBeneficiary(customerId,
				foodItemRecord);
		Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		Assertions.assertEquals(expectedResponseDTO, responseEntity.getBody());
	}

	@Test
	void testSearchMenu_Success() {
		String searchValue = "burger";
		List<FoodItem> expectedFoodItems = new ArrayList<>();
		expectedFoodItems
				.add(new FoodItem("FID123", "Burger", "Tasty burger", "Fast Food", 8.99, "Burger Place", null));
		when(foodItemService.searchMenu(searchValue)).thenReturn(expectedFoodItems);
		List<FoodItem> actualFoodItems = foodItemController.searchMenu(searchValue);
		Assertions.assertEquals(expectedFoodItems.size(), actualFoodItems.size());
		Assertions.assertEquals(expectedFoodItems.get(0).getFoodName(), actualFoodItems.get(0).getFoodName());
	}

	@Test
	void testSearchMenu_NotFound() {
		String searchValue = "nonexistent";
		when(foodItemService.searchMenu(searchValue)).thenReturn(new ArrayList<>());
		Assertions.assertThrows(NotFoundException.class, () -> {
			foodItemController.searchMenu(searchValue);
		});
	}
}