package com.food.controllerTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.food.DTO.OrdersDTO;
import com.food.controller.OrderController;
import com.food.service.OrderService;
import com.food.service.OrderService.OrderDetailsResponse;

@ExtendWith(MockitoExtension.class)
class OrderControllerTest {
	@Mock
	private OrderService orderService;
	@InjectMocks
	private OrderController orderController;
	private MockMvc mockMvc;
	private ObjectMapper objectMapper = new ObjectMapper();

	@Test
	void testPlaceOrder() throws Exception {
		OrdersDTO ordersDTO = new OrdersDTO();
		ordersDTO.setCustomerId("customerId");
		ordersDTO.setFoodId("foodId");
		ordersDTO.setQuantity(2);
		mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
		mockMvc.perform(post("/api/orders/placeOrder").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(ordersDTO))).andExpect(status().isOk())
				.andExpect(content().string("Order placed successfully"));
		verify(orderService, times(1)).placeOrder(ordersDTO);
	}

	@Test
	void testGetOrderDetails() throws Exception {
		OrderDetailsResponse orderDetailsResponse = new OrderDetailsResponse(Collections.emptyList(), 0.0);
		when(orderService.getOrderDetails("customerId")).thenReturn(orderDetailsResponse);
		mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
		mockMvc.perform(get("/api/orders/details/{customerId}", "customerId")).andExpect(status().isOk())
				.andExpect(jsonPath("$.totalAmountToPay").value(0.0));
		verify(orderService, times(1)).getOrderDetails("customerId");
	}

	@Test
	void testProcessPayment_Successful() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
		mockMvc.perform(post("/api/orders/orders/process-payment").param("customerId", "customerId")
				.param("totalAmountToPay", "20.0"));
		verify(orderService, times(1)).processPayment("customerId", 20.0);
	}

	@Test
	void testProcessPayment_Failed() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
		mockMvc.perform(post("/api/orders/orders/process-payment").param("customerId", "customerId")
				.param("totalAmountToPay", "15.0")).andExpect(status().isBadRequest())
				.andExpect(content().string("Payment failed: Total amount doesn't match"));
		verify(orderService, times(1)).processPayment("customerId", 15.0);
	}
}