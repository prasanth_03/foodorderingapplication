package com.food.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.food.DTO.CustomerRegistrationDTO;
import com.food.DTO.ResponseDTO;
import com.food.entity.CustomerRegistration;
import com.food.exception.UserAlreadyExists;
import com.food.repository.CustomerRegistrationRepository;
import com.food.service.CustomerRegistrationServiceImpl;

@SpringBootTest
public class CustomerRegistrationServiceImplTest {
	@Mock
	private CustomerRegistrationRepository registrationRepository;
	@InjectMocks
	private CustomerRegistrationServiceImpl registrationService;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testRegistration_Successful() {

		CustomerRegistrationDTO registrationDTO = new CustomerRegistrationDTO("John Doe", "john@example.com",
				"9876543210", "password");
		when(registrationRepository.findByEmail(registrationDTO.getEmail())).thenReturn(Optional.empty());
		when(registrationRepository.save(any(CustomerRegistration.class))).thenReturn(new CustomerRegistration());

		ResponseDTO response = registrationService.registration(registrationDTO);

		assertNotNull(response);

		assertEquals("User Registration successfully...", response.getMessage().get(0));
	}

	@Test
	void testRegistration_UserAlreadyExists() {

		CustomerRegistrationDTO registrationDTO = new CustomerRegistrationDTO("John Doe", "john@example.com",
				"9876543210", "password");
		when(registrationRepository.findByEmail(registrationDTO.getEmail()))
				.thenReturn(Optional.of(new CustomerRegistration()));

		assertThrows(UserAlreadyExists.class, () -> {
			registrationService.registration(registrationDTO);
		});
	}
}
