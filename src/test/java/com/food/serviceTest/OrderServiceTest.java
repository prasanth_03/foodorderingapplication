package com.food.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.food.DTO.OrdersDTO;
import com.food.DTO.PaymentStatus;
import com.food.entity.CustomerRegistration;
import com.food.entity.FoodItem;
import com.food.entity.Orders;
import com.food.exception.NotFoundException;
import com.food.repository.CustomerRegistrationRepository;
import com.food.repository.FoodItemRepository;
import com.food.repository.OrderRepository;
import com.food.service.OrderService;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
	@Mock
	private OrderRepository orderRepository;
	@Mock
	private CustomerRegistrationRepository customerRepository;
	@Mock
	private FoodItemRepository foodItemRepository;
	@InjectMocks
	private OrderService orderService;
	private OrdersDTO ordersDTO;

	@BeforeEach
	void setUp() {
		ordersDTO = OrdersDTO.builder().customerId("customerId").foodId("foodId").quantity(2).build();
	}

	@Test
	void testPlaceOrder() {
		CustomerRegistration customer = new CustomerRegistration();
		customer.setCustomerId("customerId");
		FoodItem foodItem = new FoodItem();
		foodItem.setFoodId("foodId");
		foodItem.setFoodPrice(10.0);
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.of(customer));
		when(foodItemRepository.findByFoodId("foodId")).thenReturn(Optional.of(foodItem));
		orderService.placeOrder(ordersDTO);
		verify(orderRepository, times(1)).save(any());
	}

	@Test
	void testPlaceOrder_CustomerNotFound() {
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.empty());
		assertThrows(NotFoundException.class, () -> orderService.placeOrder(ordersDTO));
	}

	@Test
	void testPlaceOrder_FoodItemNotFound() {
		CustomerRegistration customer = new CustomerRegistration();
		customer.setCustomerId("customerId");
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.of(customer));
		when(foodItemRepository.findByFoodId("foodId")).thenReturn(Optional.empty());
		assertThrows(NotFoundException.class, () -> orderService.placeOrder(ordersDTO));
	}

	@Test
	void testGetOrderDetails() {
		List<Orders> orders = new ArrayList<>();
		orders.add(new Orders());
		orders.add(new Orders());
		when(orderRepository.existsByCustomerCustomerId("customerId")).thenReturn(true);
		OrderService.OrderDetailsResponse response = orderService.getOrderDetails("customerId");
		assertNotNull(response);
		assertEquals(0, response.getOrderDTOs().size());
	}

	@Test
	void testGetOrderDetails_CustomerNotFound() {
		when(orderRepository.existsByCustomerCustomerId("customerId")).thenReturn(false);
		assertThrows(NotFoundException.class, () -> orderService.getOrderDetails("customerId"));
	}

	@Test
	void testProcessPayment() {
		List<Orders> orders = new ArrayList<>();
		Orders order1 = new Orders();
		order1.setQuantity(2);
		order1.setFoodItem(new FoodItem());
		order1.getFoodItem().setFoodPrice(10.0);
		orders.add(order1);
		when(orderRepository.existsByCustomerCustomerId("customerId")).thenReturn(true);
		when(orderRepository.findByCustomerIdAndPaymentStatus("customerId", PaymentStatus.PENDING)).thenReturn(orders);
		assertTrue(orderService.processPayment("customerId", 20.0));
	}

	@Test
	void testProcessPayment_CustomerNotFound() {
		when(orderRepository.existsByCustomerCustomerId("customerId")).thenReturn(false);
		assertThrows(NotFoundException.class, () -> orderService.processPayment("customerId", 20.0));
	}

	@Test
	void testProcessPayment_TotalAmountMismatch() {
		List<Orders> orders = new ArrayList<>();
		Orders order1 = new Orders();
		order1.setQuantity(2);
		order1.setFoodItem(new FoodItem());
		order1.getFoodItem().setFoodPrice(10.0);
		orders.add(order1);
		when(orderRepository.existsByCustomerCustomerId("customerId")).thenReturn(true);
		when(orderRepository.findByCustomerIdAndPaymentStatus("customerId", PaymentStatus.PENDING)).thenReturn(orders);
		assertFalse(orderService.processPayment("customerId", 15.0));
	}
}
