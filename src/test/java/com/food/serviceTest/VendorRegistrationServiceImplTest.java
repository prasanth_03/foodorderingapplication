package com.food.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.food.DTO.ResponseDTO;
import com.food.DTO.VendorRegistrationDTO;
import com.food.entity.VendorRegistration;
import com.food.exception.UserAlreadyExists;
import com.food.repository.VendorRegistrationRepository;
import com.food.service.VendorRegistrationServiceImpl;

@ExtendWith(MockitoExtension.class)
class VendorRegistrationServiceImplTest {
	@Mock
	VendorRegistrationRepository registrationRepository;
	@InjectMocks
	VendorRegistrationServiceImpl registrationService;

	@Test
	void testRegistration() {

		VendorRegistrationDTO registrationDto = new VendorRegistrationDTO();

		when(registrationRepository.findByEmail(registrationDto.getEmail())).thenReturn(Optional.empty());
		when(registrationRepository.save(any(VendorRegistration.class)))
				.thenAnswer(invocation -> invocation.getArgument(0));
		ResponseDTO responseDto = registrationService.registration(registrationDto);

		assertNotNull(responseDto);
	}

	@Test
	void testRegistration_Success() {
		VendorRegistrationDTO registrationDto = new VendorRegistrationDTO();
		registrationDto.setName("Test Name");
		registrationDto.setShopName("Test Shop");
		registrationDto.setEmail("test@example.com");
		registrationDto.setContactNo("1234567890");
		when(registrationRepository.findByEmail("test@example.com")).thenReturn(Optional.empty());
		ResponseDTO response = registrationService.registration(registrationDto);
		assertNotNull(response);
		assertEquals(201, response.getHttpStatus());
		assertTrue(response.getMessage().contains("Vendor Registration successfully..."));
		verify(registrationRepository, times(1)).save(any(VendorRegistration.class));
	}

	@Test
	void testRegistration_Failure_UserAlreadyExists() {
		VendorRegistrationDTO registrationDto = new VendorRegistrationDTO();
		registrationDto.setName("Test Name");
		registrationDto.setShopName("Test Shop");
		registrationDto.setEmail("test@example.com");
		registrationDto.setContactNo("1234567890");
		when(registrationRepository.findByEmail("test@example.com")).thenReturn(Optional.of(new VendorRegistration()));
		assertThrows(UserAlreadyExists.class, () -> registrationService.registration(registrationDto));
		verify(registrationRepository, never()).save(any(VendorRegistration.class));
	}
}