package com.food.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.food.DTO.LoginStatusEnum;
import com.food.DTO.Roles;
import com.food.entity.CustomerRegistration;
import com.food.entity.Login;
import com.food.entity.VendorRegistration;
import com.food.repository.CustomerRegistrationRepository;
import com.food.repository.LoginRepository;
import com.food.repository.VendorRegistrationRepository;
import com.food.service.LoginService;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {
	@Mock
	private CustomerRegistrationRepository customerRegistrationRepository;
	@Mock
	private VendorRegistrationRepository vendorRepository;
	@Mock
	private LoginRepository loginRepository;
	@InjectMocks
	private LoginService loginService;

	@Test
	void testCustomerLoginSuccess() {
		String email = "customer@example.com";
		String password = "password";
		CustomerRegistration customer = new CustomerRegistration();
		customer.setEmail(email);
		customer.setPassword(password);
		when(customerRegistrationRepository.findByEmail(email)).thenReturn(Optional.of(customer));
		when(loginRepository.findByEmailAndLoginStatus(email, LoginStatusEnum.LOGGED_IN)).thenReturn(Optional.empty());
		ResponseEntity<?> response = loginService.login(email, password);
		verify(loginRepository, times(1)).save(any(Login.class));
		verify(customerRegistrationRepository, times(1)).save(customer);
		assert response.getStatusCode() == HttpStatus.OK;
		assert response.getBody().equals("Customer login successful!");
	}

	@Test
	void testVendorLoginSuccess() {
		String email = "vendor@example.com";
		String password = "password";
		VendorRegistration vendor = new VendorRegistration();
		vendor.setEmail(email);
		vendor.setPassword(password);
		when(vendorRepository.findByEmail(email)).thenReturn(Optional.of(vendor));
		when(loginRepository.findByEmailAndLoginStatus(email, LoginStatusEnum.LOGGED_IN)).thenReturn(Optional.empty());
		ResponseEntity<?> response = loginService.login(email, password);
		verify(loginRepository, times(1)).save(any(Login.class));
		verify(vendorRepository, times(1)).save(vendor);
		assert response.getStatusCode() == HttpStatus.OK;
		assert response.getBody().equals("Vendor login successful!");
	}

	@Test
	void testUserAlreadyLoggedIn() {
		String email = "customer@example.com";
		String password = "password";
		CustomerRegistration customer = new CustomerRegistration();
		customer.setEmail(email);
		customer.setPassword(password);
		Login loggedInUser = new Login(email, password, Roles.USER, LoginStatusEnum.LOGGED_IN);
		when(customerRegistrationRepository.findByEmail(email)).thenReturn(Optional.of(customer));
		when(loginRepository.findByEmailAndLoginStatus(email, LoginStatusEnum.LOGGED_IN))
				.thenReturn(Optional.of(loggedInUser));
		ResponseEntity<?> response = loginService.login(email, password);
		assert response.getStatusCode() == HttpStatus.BAD_REQUEST;
		assert response.getBody().equals("User already logged in");
	}
	 
		@Test
		void testLogin_Customer_Success() {
			// Mock behavior for customer login
			String email = "test@example.com";
			String password = "password123";
			CustomerRegistration customer = mock(CustomerRegistration.class);
			when(customer.getPassword()).thenReturn(password);
			Optional<CustomerRegistration> customerOptional = Optional.of(customer);
			when(customerRegistrationRepository.findByEmail(email)).thenReturn(customerOptional);
			when(loginRepository.save(any(Login.class))).thenReturn(new Login());
	 
			ResponseEntity<?> responseEntity = loginService.login(email, password);
	 
			verify(customerRegistrationRepository).findByEmail(email);
			verify(loginRepository).save(any(Login.class));
			verify(customer).setLoginStatus(LoginStatusEnum.LOGGED_IN);
			verify(customerRegistrationRepository).save(customer);
	 
			assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
			assertEquals("Customer login successful!", responseEntity.getBody());
		}
	 
		@Test
		void testLogin_Vendor_Success() {
	 
			String email = "test@example.com";
			String password = "password123";
			VendorRegistration vendor = mock(VendorRegistration.class);
			when(vendor.getPassword()).thenReturn(password);
			Optional<VendorRegistration> vendorOptional = Optional.of(vendor);
			// Optional<VendorRegistration> vendorOptional;
			when(vendorRepository.findByEmail(email)).thenReturn(vendorOptional);
			when(loginRepository.save(any(Login.class))).thenReturn(new Login());
	 
			ResponseEntity<?> responseEntity = loginService.login(email, password);
	 
			verify(vendorRepository).findByEmail(email);
			verify(loginRepository).save(any(Login.class));
			verify(vendor).setLoginStatus(LoginStatusEnum.LOGGED_IN);
			verify(vendorRepository).save(vendor);
	 
			assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
			assertEquals("Vendor login successful!", responseEntity.getBody());
		}
	 
		@Test
		void testLogin_InvalidCredentials() {
	 
			when(customerRegistrationRepository.findByEmail("test@example.com")).thenReturn(Optional.empty());
			when(vendorRepository.findByEmail("test@example.com")).thenReturn(Optional.empty());
	 
			ResponseEntity<?> responseEntity = loginService.login("test@example.com", "password123");
	 
			// verify(loginRepository, never()).findByEmailAndLoginStatus(anyString(),
			// any());
			 verify(customerRegistrationRepository).findByEmail("test@example.com");
			 verify(vendorRepository).findByEmail("test@example.com");
	 
			assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
			assertEquals("Invalid email/password", responseEntity.getBody());
		}
}