package com.food.serviceTest;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.food.DTO.FoodItemRecord;
import com.food.DTO.LoginStatusEnum;
import com.food.DTO.VendorResponseDTO;
import com.food.entity.FoodItem;
import com.food.entity.VendorRegistration;
import com.food.exception.ResourceNotFound;
import com.food.repository.FoodItemRepository;
import com.food.repository.VendorRegistrationRepository;
import com.food.service.FoodItemService;

class FoodItemServiceTest {
	@Mock
	private FoodItemRepository foodItemRepository;
	@Mock
	private VendorRegistrationRepository registrationRepository;
	@InjectMocks
	private FoodItemService foodItemService;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testAddMenu_Success() {
		FoodItemRecord foodItemRecord = new FoodItemRecord("Pizza", "Delicious pizza", "Italian", 15.99);
		VendorRegistration registration = new VendorRegistration("vendor123", "vendor@example.com", "password",
				"Pizza Shop", LoginStatusEnum.LOGGED_IN);
		when(registrationRepository.findByVendorIdAndLoginStatus("vendor123", LoginStatusEnum.LOGGED_IN))
				.thenReturn(Optional.of(registration));
		when(foodItemRepository.existsByFoodName("Pizza")).thenReturn(false);
		VendorResponseDTO responseDTO = foodItemService.addMenu("vendor123", foodItemRecord);
		Assertions.assertEquals("Food details Added Successfully...", responseDTO.getMessage());
		verify(foodItemRepository, times(1)).save(any());
	}

	@Test
	void testAddMenu_FoodAlreadyExists() {
		FoodItemRecord foodItemRecord = new FoodItemRecord("Pizza", "Delicious pizza", "Italian", 15.99);
		VendorRegistration registration = new VendorRegistration("vendor123", "vendor@example.com", "password",
				"Pizza Shop", LoginStatusEnum.LOGGED_IN);
		when(registrationRepository.findByVendorIdAndLoginStatus("vendor123", LoginStatusEnum.LOGGED_IN))
				.thenReturn(Optional.of(registration));
		when(foodItemRepository.existsByFoodName("Pizza")).thenReturn(true);
		Assertions.assertThrows(ResourceNotFound.class, () -> {
			foodItemService.addMenu("vendor123", foodItemRecord);
		});
		verify(foodItemRepository, never()).save(any());
	}

	@Test
	void testSearchMenu_Success() {
		List<FoodItem> expectedFoodItems = new ArrayList<>();
		expectedFoodItems
				.add(new FoodItem("FID123", "Burger", "Tasty burger", "Fast Food", 8.99, "Burger Place", null));
		when(foodItemRepository
				.findByFoodNameContainingIgnoreCaseOrFoodTypeContainingIgnoreCaseOrShopNameContainingIgnoreCase(
						"burger", "burger", "burger"))
				.thenReturn(expectedFoodItems);
		List<FoodItem> actualFoodItems = foodItemService.searchMenu("burger");
		Assertions.assertEquals(expectedFoodItems.size(), actualFoodItems.size());
		Assertions.assertEquals(expectedFoodItems.get(0).getFoodName(), actualFoodItems.get(0).getFoodName());
	}
}